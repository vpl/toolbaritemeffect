﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace ToolbarItemEffect
{
    public class DebugEffect : RoutingEffect
    {
        public DebugEffect() : base ("ToolbarItemEffect.DebugEffect")
        {
            Debug.WriteLine("DebugEffect (RoutingEffect) created");
        }
    }
}
