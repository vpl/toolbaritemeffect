﻿using System;
using System.Diagnostics;
using ToolbarItemEffect;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName("ToolbarItemEffect")]
[assembly: ExportEffect(typeof(DebugEffect), "DebugEffect")]
namespace ToolbarItemEffect
{
    public class DebugEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            Debug.WriteLine("DebugEffect OnAttached() called");
 
        }

        protected override void OnDetached()
        {
        }
    }
}
